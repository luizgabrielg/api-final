const mongodb = require('../../lib/mongodb')
const util = require('../../lib/util')
const collectionProdutos = 'produtos'
const { ObjectId } = require('mongodb')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body._id) return util.bind(new Error('Insira o ID'))
    if (!body.nome) return util.bind(new Error('Insira um nome para o produto'))
    if (!body.preco) return util.bind(new Error('Insira um preço para o produto'))
    if (!body.quantidade) return util.bind(new Error('Insira a quantidade do produto'))

    await mongodb.connect()

    const produtos = {
      nome: body.name,
      preco: body.email,
      quantidade:body.quantidade
    }

    await mongodb(collectionProdutos).updateOne(
      {
        _id: ObjectId(body._id)
      },
      {
        $set: produtos
      }
    )
    return util.bind({})
  } catch (error) {
    return util.bind(error)
  }
}
