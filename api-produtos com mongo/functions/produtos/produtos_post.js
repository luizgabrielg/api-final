const mongodb = require('../../lib/mongodb')
const util = require('../../lib/util')
const collectionProdutos = 'produtos'

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.nome) return util.bind(new Error('Insira um nome para o produto'))
    if (!body.preco) return util.bind(new Error('Insira um preço para o produto'))
    if (!body.quantidade) return util.bind(new Error('Insira a quantidade do produto'))

    await mongodb.connect()

    const checkprodutoExist = await mongodb(collectionProdutos).findOne({ nome: body.name })
    if (checkprodutoExist) return util.bind(new Error('Este produto já esta cadastrado!'))

    const produtos = {
      nome: body.name,
      preco: body.email,
      quantidade:body.quantidade
    }

    await mongodb(collectionProdutos).insertOne(produtos)
    return util.bind({})
  } catch (error) {
    return util.bind(error)
  }
}
