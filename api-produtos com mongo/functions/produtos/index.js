const get = require('./produtos_get.js')
const post = require('./produtos_post.js')
const put = require('./produtos_put.js')
const remove = require('./produtos_remove.js')

module.exports = {
  get,
  post,
  put,
  remove
}
