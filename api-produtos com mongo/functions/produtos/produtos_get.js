const mongodb = require('../../lib/mongodb')
const util = require('../../lib/util')
const { ObjectId } = require('mongodb')
const collectionProdutos = 'produtos'

module.exports = async (event) => {
  try {
    await mongodb.connect()
    const perPage = 50
    const page = event.queryStringParameters && event.queryStringParameters.page ? event.queryStringParameters.page : 1

    if (event.pathParameters && event.pathParameters.id) {
      const produto = await mongodb(collectionProdutos).findOne({ _id: ObjectId(event.pathParameters.id) })
      return util.bind(produto)
    }

    const produtos = await mongodb(collectionProdutos).find({}).skip(perPage * page - perPage).limit(perPage).toArray()
    return util.bind(produtos)
  } catch (error) {
    return util.bind(error)
  }
}
