'use strict'

require('dotenv').config()
const path = require('path')

const produtos = require(path.join(__dirname, 'functions', 'produtos'))

module.exports = {
  produtos: (event, context) => {
    if (event.resource === '/produtos' && event.httpMethod === 'GET') return produtos.get(event, context)
    if (event.resource === '/produtos' && event.httpMethod === 'POST') return produtos.post(event, context)
    if (event.resource === '/produtos' && event.httpMethod === 'PUT') return produtos.put(event, context)
    if (event.resource === '/produtos' && event.httpMethod === 'DELETE') return produtos.remove(event, context)
    if (event.resource === '/produtos/{id}' && event.httpMethod === 'GET') return produtos.get(event, context)
  }
}
