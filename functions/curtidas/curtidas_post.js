const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id_usuario) return util.bind(new Error('Enter your id_usuario!'))
    if (!body.id_post) return util.bind(new Error('Enter your id_post!'))
    if (!body.datahora) return util.bind(new Error('Enter your datahora!'))
  

    if (checkUserExist.length) return util.bind(new Error('An post already exists!'))

    const insert = await mysql.query('insert into curtidas (id_usuario, id_post, datahora) values (?,?,?)', [body.id_usuario, body.id_post, body.datahora])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
