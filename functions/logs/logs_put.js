const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id) return util.bind(new Error('Enter your code!'))
    if (!body.log) return util.bind(new Error('Enter your log!'))
    await mysql.query('update logs set log=? where id=?', [body.id, body.log])
    return util.bind({})
  } catch (error) {
    return util.bind(error)
  }
}
