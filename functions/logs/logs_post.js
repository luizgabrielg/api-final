const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id_usuario) return util.bind(new Error('Enter your id_usuario!'))
    if (!body.log) return util.bind(new Error('Enter your log!'))
    if (!body.datahora) return util.bind(new Error('Enter your datahora!'))
  
    if (checkUserExist.length) return util.bind(new Error('An account with this email already exists!'))

    const insert = await mysql.query('insert into logs (id_usuario, log, datahora) values (?,?,?)', [body.id_usuario, body.log, body.datahora])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
