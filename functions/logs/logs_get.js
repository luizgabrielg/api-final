const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const user = await mysql.query('select * from logs where id=?', [event.pathParameters.id])
      return util.bind(user.length ? user[0] : {})
    }

    const logs = await mysql.query('select * from logs')
    return util.bind(logs)
  } catch (error) {
    return util.bind(error)
  }
}
