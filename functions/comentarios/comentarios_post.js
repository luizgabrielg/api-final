const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id_usuario) return util.bind(new Error('Enter your id_usuario!'))
    if (!body.id_post) return util.bind(new Error('Enter your titulo!'))
    if (!body.comentario) return util.bind(new Error('Enter your image!'))
    if (!body.datahora) return util.bind(new Error('Enter your datahora!'))

  
    if (checkUserExist.length) return util.bind(new Error('An post already exists!'))

    const insert = await mysql.query('insert into comentarios (id_usuario, id_post, comentario, datahora) values (?,?,?,?)', [body.id_usuario, body.id_post, body.comentario, body.datahora])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
